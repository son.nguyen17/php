                                            <!-- Chuỗi trong PHP -->
<!-- In ra độ dài chuỗi -->
<?php
$str = "Hello guy,this is the first day we learn php!";
$length = strlen($str);
echo $str;
echo "<br>";
echo "Độ dài chuỗi trên là:".$length;
echo "<br>";
?>

<!-- In ra số từ trong chuỗi -->
<?php
$str = "Hello guy,this is the first day we learn php!";
$count = str_word_count($str);
echo $str;
echo "<br>";
echo "Số lượng từ chuỗi trên là:".$count;
echo "<br>";
?>

<!-- Tìm vị trí từ guy trong chuỗi trên -->
<?php
$str = "Hello guy,this is the first day we learn php!";
$pos = strpos($str,'guy');
echo $str;
echo "<br>";
echo "Vị trí từ guy là:".$pos;
echo "<br>";
?>

<!-- Thay thế từ guy bằng tên của bạn -->
<?php
$str = "Hello guy,this is the first day we learn php!";
$replace = str_replace('guy','Son',$str);
echo $str;
echo "<br>";
echo "Thay thế từ guy bằng Son:".$replace;
?>

