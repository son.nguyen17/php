<!-- Gộp 2 mảng -->
<?php
$a = [1,2,3,7,8,9,4,5,6];
$b = [10,20,30,70,80,90,40,50,60];
$c = array_merge($a,$b);
print_r($c);
echo "<br>";
?>

<!-- In ra số phần tử của mảng $c -->
<?php
$a = [1,2,3,7,8,9,4,5,6];
$b = [10,20,30,70,80,90,40,50,60];
$c = array_merge($a,$b);
echo "Số phần tử trong mảng là:".count($c);
echo "<br>";
?>

<!-- In ra  phần tử lớn nhất mảng $c -->
<?php
$a = [1,2,3,7,8,9,4,5,6];
$b = [10,20,30,70,80,90,40,50,60];
$c = array_merge($a,$b);
echo "Số phần tử lớn nhất trong mảng là:".max($c);
echo "<br>";
?>

<!-- In ra số phần tử nhỏ nhất trong mảng $c -->
<?php
$a = [1,2,3,7,8,9,4,5,6];
$b = [10,20,30,70,80,90,40,50,60];
$c = array_merge($a,$b);

echo "Số phần tử nhỏ nhất trong mảng là:".min($c);
echo "<br>";
echo "Vị trí phần tử có giá trị 40 là:".array_search('40',$c);
echo "<br>";
echo "Sắp xếp mảng theo thứ tự từ lớn đến bé:";
rsort($c);
foreach( $c as $number) {
    echo "$number,";
}
echo "<br>";
?>

<!-- Chuyển mảng thành định dạng chuỗi json và in ra màn hình -->
<?php
$a = [1,2,3,7,8,9,4,5,6];
$b = [10,20,30,70,80,90,40,50,60];
$c = array_merge($a,$b);
echo json_encode($c);
?>




